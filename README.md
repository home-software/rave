### If you're not on <https://git.kroner.dev/home-software/rave>, then you're on a mirror.

# rave

a simple command-line program for launching an RTSP server and sharing your
default sink audio

this program was created when I haven't had a 3.5mm jack on my computer usable,
so I had to route my audio via the home network onto my phone, and then to the
headphones

heavily inspired by [Audio Sharing](https://gitlab.gnome.org/World/AudioSharing)

uses opusenc instead of vorbisenc (should result in better quality _and_
latency)

on android, I recommend using mpv, as I haven't been able to make VLC work
without constant hitching

recommended `mpv.conf`:

```
profile=low-latency
untimed=yes
cache=no
cache-secs=0
demuxer-readahead-secs=0
cache-pause=no
```
