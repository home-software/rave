use gstreamer::{prelude::*, DeviceMonitor};
use gstreamer_rtsp_server::{prelude::*, RTSPMediaFactory, RTSPServer};
use network_interface::{Addr, NetworkInterface, NetworkInterfaceConfig};

const ENDPOINT: &str = "/audio";

fn main() {
    gstreamer::init().expect("failed to init gstreamer!");

    let dev_monitor = DeviceMonitor::new();
    dev_monitor
        .start()
        .expect("failed to start device monitor!");

    let devices = dev_monitor.devices().collect::<Vec<_>>();

    dev_monitor.stop();

    let matching_device = devices
        .into_iter()
        .find(|d| {
            d.device_class() == "Audio/Sink"
                && d.properties()
                    .expect("no device properties!")
                    .get("is-default")
                    .expect("failed to get is-default param")
        })
        .expect("no default sink found!");

    let sink_name = matching_device.display_name();

    let server = RTSPServer::new();
    server.connect_client_connected(|_server, _client| println!("client connected!"));

    let factory = RTSPMediaFactory::new();
    let launch = format!("pulsesrc device={sink_name}.monitor client-name=rave ! opusenc frame-size=2.5 ! rtpopuspay name=pay0 pt=96");

    println!("{launch}");
    factory.set_launch(&launch);
    factory.set_shared(true);

    let mounts = server.mount_points().unwrap();
    mounts.add_factory(ENDPOINT, &factory);

    let ctx = glib::MainContext::default();
    let main_loop = glib::MainLoop::new(Some(&ctx), false);

    server
        .attach(Some(&ctx))
        .expect("failed to attach glib context");

    let addresses = NetworkInterface::show()
        .expect("failed to get network interfaces!")
        .into_iter()
        .filter_map(|iface| match iface.name.as_str() {
            "lo" => None,
            _ => match iface.addr {
                Some(Addr::V4(address)) => {
                    Some(format!("rtsp://{}:8554{ENDPOINT}", address.ip.to_string()))
                }
                _ => None,
            },
        })
        .collect::<Vec<_>>();

    println!(
        "serving {sink_name} at addresses:\n{}",
        addresses.join("\n")
    );

    main_loop.run()
}
